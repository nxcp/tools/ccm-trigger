package main

import (
	"context"
	"flag"
	"fmt"
	"time"

	"github.com/golang/glog"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/klog"
)

var (
	kubeconfig = flag.String("kubeconfig", "incluster", "absolute path to the kubeconfig file")
)

func main() {
	flag.Parse()
	var config *rest.Config
	var err error
	if *kubeconfig == "incluster" {
		config, err = rest.InClusterConfig()
		if err != nil {
			panic(err.Error())
		}
	} else {
		config, err = clientcmd.BuildConfigFromFlags("", *kubeconfig)
		if err != nil {
			glog.Errorln(err)
		}
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Errorln(err)
	}

	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(clientset, time.Second*30)
	epInformer := kubeInformerFactory.Core().V1().Endpoints().Informer()

	svcInformer := kubeInformerFactory.Core().V1().Services().Informer()

	epInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			// ignore this
		},
		DeleteFunc: func(obj interface{}) {
			// ignore this
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			new := newObj.(*v1.Endpoints)
			old := oldObj.(*v1.Endpoints)

			if len(new.Subsets) > 0 {
				if len(old.Subsets) > 0 {
					if new.Subsets[0].Addresses[0].NodeName != nil {
						node := make(map[string]bool)
						if old.Subsets[0].Addresses[0].NodeName != nil {
							for _, addr := range old.Subsets[0].Addresses {
								node[*addr.NodeName] = true
							}
						}
						for _, addr := range new.Subsets[0].Addresses {
							if !node[*addr.NodeName] {
								svcInf, _, _ := svcInformer.GetIndexer().GetByKey(fmt.Sprintf("%s/%s", new.Namespace, new.Name))
								svc := svcInf.(*v1.Service)
								if svc.Spec.Type == "LoadBalancer" {
									err := PatchSVC(clientset, new.Namespace, new.Name)
									if err != nil {
										klog.Errorf("Error Patch service %s , %s : %s ", new.Namespace, new.Name, err.Error())
									}
								}
							}
						}
					}
				} else {
					if new.Subsets[0].Addresses[0].NodeName != nil {
						svcInf, _, _ := svcInformer.GetIndexer().GetByKey(fmt.Sprintf("%s/%s", new.Namespace, new.Name))
						svc := svcInf.(*v1.Service)
						if svc.Spec.Type == "LoadBalancer" {
							err := PatchSVC(clientset, new.Namespace, new.Name)
							if err != nil {
								klog.Errorf("Error Patch service %s , %s : %s ", new.Namespace, new.Name, err.Error())
							}
						}
					}
				}
			}
		},
	})

	stop := make(chan struct{})
	defer close(stop)
	kubeInformerFactory.Start(stop)
	for {
		time.Sleep(time.Second)
	}
}

func PatchSVC(client *kubernetes.Clientset, namespace string, name string) error {
	_, err := client.CoreV1().Services(namespace).Patch(context.TODO(), name, types.JSONPatchType, []byte(`[
		{"op": "replace",
		 "path": "/metadata/annotations/ccm",
		 "value": "`+time.Now().Format(time.RFC3339)+`"}]
	  `), metav1.PatchOptions{})

	return err
}

module ccm-trigger

go 1.16

require (
	github.com/golang/glog v1.0.0
	k8s.io/api v0.22.2
	k8s.io/apimachinery v0.22.2
	k8s.io/client-go v0.22.2
	k8s.io/klog v1.0.0
)

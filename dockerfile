FROM golang:alpine as builder
RUN mkdir /build 
ADD . /build/
WORKDIR /build 
RUN go build -o main cmd/main/main.go

FROM zoftdev/alpine-bkk
RUN adduser -S -D -H -h /app appadm
USER appadm
COPY --from=builder /build/main /app/
WORKDIR /app
CMD ["./main"]